# Salesforce.com + Pointzi  Mobile SDK for iOS
[![CircleCI](https://circleci.com/gh/forcedotcom/SalesforceMobileSDK-iOS/tree/dev.svg?style=svg)](https://circleci.com/gh/forcedotcom/SalesforceMobileSDK-iOS/tree/dev)
[![codecov](https://codecov.io/gh/forcedotcom/SalesforceMobileSDK-iOS/branch/dev/graph/badge.svg)](https://codecov.io/gh/forcedotcom/SalesforceMobileSDK-iOS/branch/dev)
[![Known Vulnerabilities](https://snyk.io/test/github/forcedotcom/SalesforceMobileSDK-iOS/badge.svg)](https://snyk.io/test/github/forcedotcom/SalesforceMobileSDK-iOS)
![GitHub release (latest SemVer)](https://img.shields.io/github/v/release/forcedotcom/SalesforceMobileSDK-iOS?sort=semver)


You have arrived at the source repository for the Salesforce Mobile SDK Integration with Pointzi SDK for iOS.  

Salesforce Mobile SDK 
==
Original repo for Salesforce Mobile SDK  is located at https://github.com/forcedotcom/SalesforceMobileSDK-iOS.git


Installation
==
## Setting up the repo
First, clone the repo:

- Open the Terminal App
- `git clone https://gitlab.com/pointzi/samples/ios/SalesforceMobileSDK-iOSApp.git`

After cloning the repo:

- `cd SalesforceMobileSDK-iOS/native/SampleApps/RestAPIExplorer`
- `open RestAPIExplorer.xcworkspace`
-  `pod update`
-  Run

Pointzi SDK
==
* [Pointzi](https://pointzi.com/)
* [Pointzi Integration](https://dashboard.pointzi.com/docs/sdks/ios/integration/)
* [Pointzi Changelogs](https://dashboard.pointzi.com/docs/changelogs/ios/)
